package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    protected ArvoreBinariaPesquisa<E> pai;

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        if (valor.equals('X') == false && valor.equals('Y') == false && valor.equals('Z') == false){
            ArvoreBinariaPesquisa<E> i = new ArvoreBinariaPesquisa<>(valor);
            return i;
        }
        else return null;
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() {
        ArvoreBinariaPesquisa<Character> i = new ArvoreBinariaPesquisa<>('A');
        return (ArvoreBinariaPesquisa<E>) i;
    }

    /**
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<Character> i = new ArvoreBinariaPesquisa<>('N');
        return (ArvoreBinariaPesquisa<E>) i;
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        if (no.valor.equals('A'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('C');
        if (no.valor.equals('C'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('D');
        if (no.valor.equals('D'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('F');
        if (no.valor.equals('F'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('G');
        if (no.valor.equals('G'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('H');
        if (no.valor.equals('H'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('J');
        if (no.valor.equals('J'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('M');
        if (no.valor.equals('M'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('N');
        if (no.valor.equals('N'))
            return null;
        else
            return null;
    }

    /**
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        if (no.valor.equals('A'))
            return null;
        if (no.valor.equals('C'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('A');
        if (no.valor.equals('D'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('C');
        if (no.valor.equals('F'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('D');
        if (no.valor.equals('G'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('F');
        if (no.valor.equals('H'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('G');
        if (no.valor.equals('J'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('H');
        if (no.valor.equals('M'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('J');
        if (no.valor.equals('N'))
            return (ArvoreBinariaPesquisa<E>) new ArvoreBinariaPesquisa<Character>('M');
        else
            return null;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
        return this;
    }

    /**
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        return this;
    }
}
